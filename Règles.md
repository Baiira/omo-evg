
# OMO - EVG

## Règles

Oh mon dieu, des gobelins ont foutu le merdier et ont cassé le mot magique !
Nos magiciens ont réussis à trouver le nombre de lettre et l'ordre des lettres.
Ils ont fait en sorte d'afficher leur découverte dans la page.
De ce qu'ils ont pu déterminé le mot magique se trouve dans la div avec l'id: "réponse"
Nous comptons sur vous pour reconstituer le mot !

- Reconstituer le mot dans l'ordre
- Il n'est possible d'ouvrir et modifier que le fichier allowed.css
- L'ordre est déterminé par les couleurs
- 1 shot pour chaque rafraichissement
- 1 shot pour donner une réponse
- 1 shot pour chaque mauvaise réponse donné
- 1 shot pour pouvoir faire une recherche sur internet, il n'est possible que de cliquer qu'un seul lien, peu importe la pértinence de la réponse
- 1 shot pour fêter la bonne réponse
- La réponse doit être affiché sur l'écran après un rafraichissement pour être valide
