let frameFuncs = [];

export function push(frameFunction) {
  queueMicrotask(function () {
    frameFuncs.push(frameFunction);
  });
}

function* frame() {
  while (true) {
    requestAnimationFrame(function () {
      for (let i = 0; i < frameFuncs.length; i++) {
        frameFuncs[i]();
      }
      frameFuncs = [];
      frame().next();
    });
    yield;
  }
}

requestAnimationFrame(function () {
  frame().next();
});
