import * as frames from '/scripts/frames.js'
import * as sec from '/scripts/secret.js';
import * as img from '/scripts/image.js';
import * as dialog from '/scripts/dialog.js';

(function () {
  'use strict';
  const title = document.getElementById('title');
  title.innerText = self.crypto.randomUUID();
  const encoded = sec.formatSecret();
  const pos = sec.getWordPrint();

  const response = document.getElementById('res');
  for (const c of pos) {
    const div = document.createElement('div');
    div.classList.add('henqrkj', c);
    response.appendChild(div);
  }

  const text = document.getElementById('réponse');
  for (const c of encoded) {
    const parent = document.createElement('div');
    const wrap = document.createElement('div');
    const letter = document.createElement('div');
    letter.classList.add(c[0]);
    wrap.classList.add('henqrkj', c[1], c[2]);
    wrap.appendChild(letter);
    parent.appendChild(wrap);
    text.appendChild(parent);
  }

  frames.push(function () {
    // img.imageAuto();
  });
})();
