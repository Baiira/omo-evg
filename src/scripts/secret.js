const params = new URLSearchParams(window.location.search);
const secret = atob(params.get('secret')).toUpperCase();
let seed = parseInt(params.get('seed'));

const forward = [
  'bsrtbt',
  'trsjjtr',
  'bsrtstnr',
  'srtrst',
  'mnegolr',
  'ùregrme',
  'zùpoiejf',
  'ùerferz',
];

const pos = [
  'hazide',
  'zadmkj',
  'lzqhvbd',
  'zmlkbdj',
  'mjzbefkfzem',
  'azdafe',
  'fezgrz',
  'jbzml',
];

const letters = new Map([
  ['E', 'azeqs'],
  ['P', 'fsdlkj'],
  ['O', 'ljksd'],
  ['L', 'dfgpou'],
  ['C', 'sdckhvjb'],
  ['J', 'dfhgj'],
  ['H', 'jflksd'],
  ['T', 'sfdhbj'],
  ['U', 'fkjezef'],
  ['G', 'boids'],
  ['S', 'fzebiu'],
  ['F', 'jfhevb'],
  ['I', 'zedjbh'],
]);

export function getWordPrint(word = secret) {
  const classes = [];
  if (word.length > 9) {
    console.warn('word too long');
    return [];
  }
  for (let i = 0; i < word.length; i++) {
    classes.push(pos[i]);
  }
  return classes;
}

export function formatSecret(word = secret) {
  const classes = [];
  if (word.length > forward.length) {
    console.warn('word too long');
    return [];
  }
  for (let i = 0; i < word.length; i++) {
    const rand = Math.round(random() * (forward.length - 1));
    const part = word[i].toUpperCase();
    if (!letters.has(part)) {
      continue;
    }
    const c = [letters.get(part), pos[i],  forward[rand]];
    classes.push(c);
  }
  return classes;
}

function random() {
  const x = Math.sin(seed++) * 10000;
  return x - Math.floor(x);
}
