(function () {
  const dialog = document.getElementById('dialog');
  const showButton = document.getElementById('open-button');
  const closeButton = document.getElementById('close-button');

  showButton.addEventListener('click', () => {
    dialog.showModal();
  });

  closeButton.addEventListener('click', () => {
    dialog.close();
  });
}());
