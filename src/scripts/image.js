import * as frames from '/scripts/frames.js'

const maxTop = 1200;
let imgTop = maxTop;
const img = document.getElementById('image');

const imgSrc = [
  '/img/1.jpg',
  '/img/2.jpg',
  '/img/3.jpg',
  '/img/4.jpg',
  '/img/5.jpg',
  '/img/6.jpg',
];

let direction = 1;

export function imageAuto() {
  let minus = 10;
  if (imgTop < 20) {
    direction = -1;
  } else if (imgTop > maxTop) {
    direction = 1;
    frames.push(function () {
      randImage();
    });
  }
  if (imgTop < 300) {
    minus = (minus * imgTop / 300);
  }
  img.style.top = `${imgTop}px`;
  imgTop = (imgTop - direction * minus);
  frames.push(function () {
    imageAuto();
  });
}

function randImage() {
  const rand = Math.round(Math.random() * (imgSrc.length - 1));
  img.src = imgSrc[rand];
}

const options = {
  root: document.documentElement,
};
const observer = new IntersectionObserver((entries, observer) => {
  entries.forEach(() => {
    randImage();
  });
}, options);
observer.observe(img);
