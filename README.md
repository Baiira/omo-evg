
# OMO - EVG

## Préparation du test

````
npm i
````

## Lancer le test

````
npm run start
````
Le test est accessible: [Lien](http://localhost:8080/?secret=RkVTU0VT&seed=123)

Le secret est le mot à trouver encodé en b64, taille maximum 8 lettres. (dans le lien: FESSES) 

La seed est utilisée pour randomisé les classes css appliquées pour chaque lettres. Afin de s'assurer que le test est réalisable.

Un rappel des règles est accessibles dans le test.

